# 알고리즘 및 러스트 공부 프로젝트

## 개요

- Algorithms 4th edition (로버트 세지윅, 케빈 웨인 지음 | 권오인 옮김 | 길벗) 책을 공부하며 기록도 남길 겸 실습의 목적으로 본 프로젝트를 개설하였습니다.
- 작성 언어는 Rust입니다. 본래 이노베이션 아카데미가 `c`를 위주로 한다고 해서 `c` 공부 겸 알고리즘을 `c`로 구현하려고 했으나 너무 힘들었습니다. 객체지향 프로그래밍 언어가 아니다 보니까 일일히 대응되는 함수를 만들어야 하고, 어쨌든 불편했읍니더.
참고 : [이전 프로젝트 (c)](https://gitlab.com/EzKorry/algorithm-learn)

## 구동 방법

추가 예정

## 추가 조치 사항

추가 예정

## 외부 링크

- [Algorithms 4th edition 다운로드 리소스 페이지](https://algs4.cs.princeton.edu/code/)
- [데이터 파일 즉시 다운로드](https://algs4.cs.princeton.edu/code/algs4-data.zip)

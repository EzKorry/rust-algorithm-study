use std::fs::File;
use std::io::prelude::*;
use std::io::{self, BufRead};
use std::path::Path;
//use unicode_segmentation::UnicodeSegmentation as US;
/*
struct WordIterator<'a> {
  inner : &'a FileReader,
  pos: usize,
}

impl<'a> Iterator for WordIterator<'a> {
  type Item = &'a String;

  fn next(&mut self) -> Option<Self::Item> {
  }
}


impl<'a> WordIterator<'a> {
  fn new(bufReader : std::io::BufReader<std::fs::File>) -> WordIterator<'a>
  {

  }
}*/

pub struct FileReader {
  //file :File,
  // lines: io::Lines<io::BufReader<File>>,
  buf: io::BufReader<File>,
}

impl FileReader {
  /*pub fn word_iter<'a>(&'a self) -> WordIterator<'a> {
    WordIterator {
      inner:self,
      pos:0
    }
  }*/

  pub fn word_iter(self) -> impl Iterator<Item = String> {
    self
      .buf
      .lines()
      .map(|line| line.unwrap())
      .map(|line_str| line_str.split(' ').map(str::to_owned).collect::<Vec<_>>())
      .flatten()
  }

  // The output is wrapped in a Result to allow matching on errors
  // Returns an Iterator to the Reader of the lines of the file.

  pub fn from_path<P>(filename: P) -> io::Result<FileReader>
  where
    P: AsRef<Path>,
  {
    let file = File::open(filename)?;
    let buf = io::BufReader::new(file);
    // let lines = buf.lines();
    Ok(FileReader {
      //file,
      buf,
      //lines,
    })
  }

  pub fn get_first_word(self) -> Option<String> {
    self.word_iter().next()
  }

  pub fn get_lines(self) -> io::Lines<io::BufReader<File>> {
    self.buf.lines()
    // self.lines
  }

  pub fn next_line(mut self) -> io::Result<String> {
    // Ok(self.lines.next().unwrap()?)
    Ok(self.buf.lines().next().unwrap()?)
  }

  pub fn init_with_file(&self, path: &str) {
    // Create a path to the desired file
    let path = Path::new(path);
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
      // The `description` method of `io::Error` returns a string that
      // describes the error
      Err(why) => panic!("couldn't open {}: {}", display, why.to_string()),
      Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`

    let mut s = String::new();
    match file.read_to_string(&mut s) {
      Err(why) => panic!("couldn't read {}: {}", display, why.to_string()),
      Ok(_) => print!("{} contains:\n{}", display, s),
    }
    /*
    if let Ok(lines) = Self::from_path(filename: P)s(&path) {
      // Consumes the iterator, returns an (Optional) String
      for line in lines {
        if let Ok(ip) = line {
          println!("{}", ip);
        }
      }
    }*/

    // `file` goes out of scope, and the "hello.txt" file gets closed
  }

  pub fn read_str(&self) -> io::Result<String> {
    Ok(String::new())
  }

  pub fn read_int(&self) -> io::Result<i32> {
    Ok(0)
  }

  pub fn read_all_int(&self) -> io::Result<Vec<i32>> {
    Ok(vec![0])
  }

  pub fn read_all_str(&self) -> io::Result<Vec<String>> {
    Ok(vec![String::new()])
  }
}

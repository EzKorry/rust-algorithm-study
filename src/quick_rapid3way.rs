use std::fmt;
use std::cmp::Ordering;

// trait Sort {
//   pub sort()
// }
/*
#[derive(Eq)]
struct Person {
    id: u32,
    name: String,
    height: u32,
}

impl Ord for Person {
    fn cmp(&self, other: &Self) -> Ordering {
        self.height.cmp(&other.height)
    }
}

impl PartialOrd for String {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for String {
    fn eq(&self, other: &Self) -> bool {
        self.height == other.height
    }
}
*/
pub trait Key {
  fn compareTo(&self, other: &Self)  -> bool;
}

impl Key for String {
  fn compareTo(&self, other : &String) -> bool {
    other.
  }
}

pub fn less<T>(v : T, w:T) -> bool
where T: Key

{

}

pub fn show<T>(vector : &Vec<T>) 
where T : fmt::Display
{
  for t in vector.iter() {
    print!("{} ", t);
  }
  println!("");
}

pub fn is_sorted<T>(vector : &Vec<T>) -> bool 
where T: Key {

}


mod network {
  fn connect() {}
}

#[derive(Debug)]
pub struct Rectangle {
  length: u32,
  width: u32,
}

impl Rectangle {
  pub fn can_hold(&self, other: &Rectangle) -> bool {
    self.length > other.length && self.width > other.width
  }
}

pub fn add_two(a: i32) -> i32 {
  a + 2
}

#[cfg(test)]
mod tests {
  #[test]
  fn it_works() {
    assert_eq!(2 + 2, 4);
  }
  #[test]
  fn exploration() {
    assert_eq!(2 + 2, 4);
  }
  #[test]
  fn another() {
    panic!("Make this test fail");
  }
  use super::*;

  #[test]
  fn larger_can_hold_smaller() {
    let larger = Rectangle {
      length: 8,
      width: 7,
    };
    let smaller = Rectangle {
      length: 5,
      width: 1,
    };

    assert!(larger.can_hold(&smaller));
  }
  #[test]
  fn smaller_cannot_hold_larger() {
    let larger = Rectangle {
      length: 8,
      width: 7,
    };
    let smaller = Rectangle {
      length: 5,
      width: 1,
    };

    assert!(!smaller.can_hold(&larger));
  }
  #[test]
  fn it_adds_two() {
    assert_eq!(4, add_two(2));
  }
}

// trait

pub trait Summarizable {
  fn summary(&self) -> String;
}

pub struct NewsArticle {
  pub headline: String,
  pub location: String,
  pub author: String,
  pub content: String,
}

impl Summarizable for NewsArticle {
  fn summary(&self) -> String {
    format!("{}, by {} ({})", self.headline, self.author, self.location)
  }
}

pub struct Tweet {
  pub username: String,
  pub content: String,
  pub reply: bool,
  pub retweet: bool,
}

impl Summarizable for Tweet {
  fn summary(&self) -> String {
    format!("{}: {}", self.username, self.content)
  }
}

pub fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
  let mut largest = list[0];

  for &item in list.iter() {
    if item > largest {
      largest = item;
    }
  }

  largest
}

pub fn largest_ref<T>(list: &[T]) -> &T
where
  T: PartialOrd,
{
  let mut largest = &list[0];
  for item in list.iter() {
    if item > largest {
      largest = item;
    }
  }
  
  largest

}

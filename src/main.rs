use ezkorry_algorithm::*;
use std::env;
use std::fs::File;
use std::io::prelude::*;
mod read_file;
use read_file::*;

mod quick_rapid3way;
use quick_rapid3way::*;


fn main() -> Result<(), std::io::Error> {
    println!("Hello, world!");
    //read_file();
    trait_test();
    let f = FileReader::from_path("algs4-data/tinyTale.txt")?;

    let vector = f.word_iter().collect::<Vec<String>>();
    show(&vector);

   /* for word in f.word_iter() {
        println!("{}", word);
    }*/
    
    /*
    let o = if let Some(i) = f.get_first_word() {
        i
    } else {
        String::new()
    };

    println!("{}", o);
    */
    Ok(())
}

fn read_file_ho() {
    let args: Vec<String> = env::args().collect();

    let query = &args[1];
    let filename = &args[2];

    println!("Searching for {}", query);
    println!("In file {}", filename);

    let mut f = File::open(filename).expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("Something went wrong reading the file");

    println!("With text:\n{}", contents);
}

fn trait_test() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from("of course, as you probably already know, people"),
        reply: false,
        retweet: false,
    };
    println!("1 new tweet: {}", tweet.summary());

    let numbers = vec![34, 50, 25, 100, 65];

    let result = largest_ref(&numbers);
    println!("The largest_ref number is {}", result);

    let chars = vec!['y', 'm', 'a', 'q'];

    let result = largest_ref(&chars);
    println!("The largest_ref char is {}", result);
}
struct Config {
    query: String,
    filename: String,
}

fn parse_config(args: &[String]) -> Config {
    let query = args[1].clone();
    let filename = args[2].clone();

    Config { query, filename }
}
